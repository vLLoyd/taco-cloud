package sia.tacocloud.repository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import sia.tacocloud.dto.Order;
import sia.tacocloud.dto.User;

import java.util.List;

public interface OrderRepository extends CrudRepository<Order, Long> {
    List<Order> findByZip(String deliveryZip);
    List<Order> findByUserOrderByPlacedAtDesc(User user, Pageable pageable);
}
